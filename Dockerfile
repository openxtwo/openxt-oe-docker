FROM multiarch/debian-debootstrap:amd64-buster

RUN sed -i "/buster-updates/d" /etc/apt/sources.list
RUN apt-get update && apt-get install -yq \
        openssh-server openssl \
        sed wget cvs subversion git-core coreutils \
        unzip texi2html texinfo docbook-utils gawk python-pysqlite2 diffstat \
        help2man make gcc build-essential g++ desktop-file-utils chrpath cpio \
        screen bash-completion python3 iputils-ping \
        guilt iasl quilt bin86 \
        bcc libsdl1.2-dev liburi-perl genisoimage policycoreutils unzip vim \
        sudo rpm curl libncurses5-dev libncursesw5 libc6-dev-i386 libelf-dev \
        xorriso mtools dosfstools \
        file byobu man ca-certificates locales && \
        rm -rf /var/lib/apt-lists/* && \
        echo "dash dash/sh boolean false" | debconf-set-selections && \
        DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

# Download the GHC prerequisites from squeeze
RUN mkdir -p /tmp/ghc-prereq && cd /tmp/ghc-prereq && \
	wget http://archive.debian.org/debian/pool/main/g/gmp/libgmpxx4ldbl_4.3.2+dfsg-1_amd64.deb && \
	wget http://archive.debian.org/debian/pool/main/g/gmp/libgmp3c2_4.3.2+dfsg-1_amd64.deb && \
	wget http://archive.debian.org/debian/pool/main/g/gmp/libgmp3-dev_4.3.2+dfsg-1_amd64.deb && \
        dpkg -i libgmpxx4ldbl_4.3.2+dfsg-1_amd64.deb libgmp3c2_4.3.2+dfsg-1_amd64.deb \
		libgmp3-dev_4.3.2+dfsg-1_amd64.deb && \
        cd /tmp && rm -rf /tmp/ghc-prereq

# Install the required version of GHC
RUN cd /tmp && \
	wget https://downloads.haskell.org/~ghc/6.12.3/ghc-6.12.3-x86_64-unknown-linux-n.tar.bz2 && \
        tar jxf ghc-6.12.3-x86_64-unknown-linux-n.tar.bz2 && \
        rm ghc-6.12.3-x86_64-unknown-linux-n.tar.bz2 && \
        cd ghc-6.12.3 && \
        ./configure --prefix=/usr && \
        make install && \
        cd /tmp && rm -rf ghc-6.12.3

# Add "repo" tool (used by many Yocto-based projects)
RUN curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
        chmod a+x /usr/local/bin/repo

# Symlink for troublesome packages
RUN ln -s /lib64/ld-linux-x86-64.so.2 /lib/

RUN apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture)" \
    && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture).asc" \
    && gpg --verify /usr/local/bin/gosu.asc \
    && rm /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu
RUN echo '#!/bin/bash' > /usr/local/bin/entrypoint.sh 
RUN echo 'USER_ID=${LOCAL_USER_ID:-9001}' >> /usr/local/bin/entrypoint.sh 
RUN echo 'GROUP_ID=${GROUP_USER_ID:-9001}' >> /usr/local/bin/entrypoint.sh 
RUN echo 'groupadd -g ${GROUP_ID} build' >> /usr/local/bin/entrypoint.sh
RUN echo 'useradd --shell /bin/bash -u ${USER_ID} -g ${GROUP_ID} -o -c "" -m build' >> /usr/local/bin/entrypoint.sh 
RUN echo 'export HOME=/home/build' >> /usr/local/bin/entrypoint.sh
RUN echo '/usr/local/bin/gosu build "/bin/bash"' >> /usr/local/bin/entrypoint.sh 

RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
