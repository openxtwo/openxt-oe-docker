USER=$(shell whoami)
USER_ID=$(shell id -u $(USER))
GROUP_ID=$(shell id -g $(USER))

.PHONY: run
run: build
	docker run -it -e LOCAL_USER_ID=$(USER_ID) -e GROUP_USER_ID=$(GROUP_ID) -w /home/build/ -v $(HOME)/.ssh:/home/build/.ssh -v $(PWD)/build:/home/build openxt-builder:latest

.PHONY: build
build:
	mkdir -p build
	docker build -t openxt-builder - < Dockerfile
